import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPizzaComponent } from './list-pizza/list-pizza.component';
import { DetailsPizzaComponent } from './details-pizza/details-pizza.component';
import { BorderCardDirective } from '../shared/directives/border-card.directive';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
   
  ],
  imports: [
    CommonModule,
    RouterModule,
  ]
})
export class PizzaModule { }
