import { Injectable } from '@angular/core';
import { LIST_PIZZAS } from '../shared/list.pizza';
import { Pizza } from '../Pizza';

@Injectable({
  providedIn: 'root'
})
export class PizzasService {
  getListPizzas(): Pizza[]{
    return LIST_PIZZAS;
  }
  getSinglePizza(id:number): Pizza | undefined{
    const listOfPizzas = this.getListPizzas();
    return listOfPizzas.find(pizza => pizza.id === id);
  }
  getPizzaIngredients(): string []{
        return ['S. tomate','v. kebab','roquette','piments','miel','c. fraîche',
        'v. hachée','S.barbecue','champignons','merguez','mozarella','oignons'];
    }

  constructor() { }
}
