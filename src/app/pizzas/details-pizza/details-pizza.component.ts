import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pizza } from 'src/app/Pizza';
import { LIST_PIZZAS } from 'src/app/shared/list.pizza';
import { PizzasService } from '../pizzas.service';
@Component({
  selector: 'app-details-pizza',
  templateUrl: './details-pizza.component.html',
  styleUrls: ['./details-pizza.component.scss']
})

export class DetailsPizzaComponent {
  pizzaToDisplay:Pizza| undefined;

  constructor(private route :ActivatedRoute,private router:Router, private pizzaService : PizzasService){
  }

   ngOnInit():void{
    const retrievedFormURL = + this.route.snapshot.params['id'];
    this.pizzaToDisplay = this.pizzaService.getSinglePizza(retrievedFormURL);
    console.log('pizzaToDisplay :',this.pizzaToDisplay);
  }
 editPizza(selectedPizza:Pizza): void{
    // alert("Vous avez selectionné : " + selectedPizza.name)
    const link = ['pizza/edit',selectedPizza.id];
    this.router.navigate(link);
  } 
}
