import {Component, OnInit} from '@angular/core';
import { Pizza } from 'src/app/Pizza';
import { Router } from '@angular/router';
import { PizzasService } from '../pizzas.service';

@Component({
  selector: 'app-list-pizza',
  templateUrl: './list-pizza.component.html',
  styleUrls: ['./list-pizza.component.scss']
})
export class ListPizzaComponent implements  OnInit{
  PIZZAS :Pizza[] = []
  
  constructor(private router :Router, private pizzaService : PizzasService){

  }
  
  ngOnInit():void{
    this.PIZZAS = this.pizzaService.getListPizzas();
  }
  /**
   * Affichage du nom de la pizza selectionne
   * @param selectedPizza 
   */
  selectPizza(selectedPizza:Pizza): void{
    // alert("Vous avez selectionné : " + selectedPizza.name)
    const link = ['pizza',selectedPizza.id];
    this.router.navigate(link);
  }
}
