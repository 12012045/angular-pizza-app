import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PizzaFormComponent } from './pizzas/pizza-form/pizza-form.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { DetailsPizzaComponent } from './pizzas/details-pizza/details-pizza.component';
import { ListPizzaComponent } from './pizzas/list-pizza/list-pizza.component';
import { BorderCardDirective } from './shared/directives/border-card.directive';
import { FormsModule } from '@angular/forms';
import { PizzaEditComponent } from './pizzas/pizza-edit/pizza-edit.component';
import { PizzaIngredientColorPipe } from './pizzas/pizza-ingredient-color.pipe';

@NgModule({
  declarations: [
    AppComponent,
    PizzaFormComponent,
    PageNotFoundComponent,
    ListPizzaComponent,
    DetailsPizzaComponent,
    BorderCardDirective,
    PizzaEditComponent,
    PizzaIngredientColorPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
